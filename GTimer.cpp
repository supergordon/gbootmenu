#include "GSDK.h"

GTimer::Measure::Measure()
{
        start = 0;
        stopped_at = 0;
}

void GTimer::Measure::Start()
{
        start = GTimer::GetTickCount();
}

void GTimer::Measure::Reset()
{
        Measure();
        Start();
}

void GTimer::Measure::Stop()
{
        stopped_at = ElapsedTime();
        start = 0;
}

unsigned int GTimer::Measure::ElapsedTime()
{
        if (start == 0)
                return 0;

        return GTimer::GetTickCount() - start;
}

unsigned int GTimer::Measure::ElapsedStopTime()
{
        return stopped_at;
}

unsigned int GTimer::Measure::InitialTime()
{
        return start;
}

int GTimer::GetTickCount()
{
        timeval tv;
        timespec ts;
        if (clock_gettime(CLOCK_MONOTONIC, &ts) != 0)
                return 0;
        
        return static_cast<int>((ts.tv_sec * 1000) + (ts.tv_nsec / 1000 / 1000));
}

GTimer::GTimer(int value)
{
        msec = value;
        started = false;
        stopped = false;
}

void GTimer::Start()
{
        m.Start();
        
        started = true;
        stopped = false;
}

bool GTimer::Started()
{
        return started;
}

bool GTimer::Finished()
{
        if (started) {
                if (!Running()) {
                        started = false;
                        return true;
                }
        }
        return false;
}

bool GTimer::Running()
{
        if (msec >= m.ElapsedTime())
                return true;
        
        started = false;
        
        return false;
}

void GTimer::Restart()
{
        m.Stop();
        Start();
}

void GTimer::Stop()
{
        m.Stop();
        started = false;
        stopped = false;
}