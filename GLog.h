#ifndef __GLOG__
#define __GLOG__

#include "GSDK.h"

class GLog {
public:
        GLog();
        ~GLog();
	
        void Console(const char *text, ...);
        void Screen(const char *text, ...);
        void File(const char *text, ...);
        
        void GetTime(char *out);
	
        void ScreenRender();
        
        std::vector<char*> v;
        std::vector<char*>::iterator v_it;
};

extern GLog *g_pLog;

#endif