#include "GSDK.h"

GDevice *g_pDevice = new GDevice();

GDevice::GDevice()
{
        max_brightness = 0;
        cur_brightness = 0;
        old_brightness = 0;
        is_emulator = 0;
}

void GDevice::Init()
{
        /*FILE *file_max_brightness = fopen(PATH_LCD_MAX_BRIGHTNESS, "r");
        if(file_max_brightness) {
                fscanf(file_max_brightness, "%d", &max_brightness);
                fclose(file_max_brightness);
        }*/
        
        GetLcdBrightness();
}

int GDevice::GetLcdBrightness()
{
        FILE *file_brightness = fopen(PATH_LCD_BRIGHTNESS, "r");
        if(file_brightness) {
                fscanf(file_brightness, "%d", &cur_brightness);
                fclose(file_brightness);
        }
        
        return (float)cur_brightness / 2.55;
}

void GDevice::SetLcdBrightness(int percentage)
{
        FILE *file_brightness = fopen(PATH_LCD_BRIGHTNESS, "w");
        if(file_brightness) {
                fprintf(file_brightness, "%d", int(2.55 * percentage));
                fclose(file_brightness);
        }
}

void GDevice::SetLcdPowerstate(bool on)
{
        if (!on) {
                if (!old_brightness)
                        old_brightness = GetLcdBrightness();

                SetLcdBrightness(0);
        }
        else {
                SetLcdBrightness(old_brightness ? old_brightness : 100);
                cur_brightness = old_brightness;
                old_brightness = 0;
        }
}

void GDevice::UpdateStats()
{
        if (is_emulator)
                return;
        
        FILE *file_voltage = fopen(PATH_BAT_VOLTAGE, "r");
        if(file_voltage) {
                fscanf(file_voltage, "%d", &battery_voltage);
                fclose(file_voltage);
        }
        
        FILE *file_batttemp = fopen(PATH_BAT_TEMP, "r");
        if(file_batttemp) {
                fscanf(file_batttemp, "%d", &battery_temp);
                fclose(file_batttemp);
        }
        
        FILE *file_capacity = fopen(PATH_BAT_CAPACITY, "r");
        if(file_capacity) {
                fscanf(file_capacity, "%d", &battery_capacity);
                fclose(file_capacity);
        }
        
        FILE *file_cputemp = fopen(PATH_CPU_TEMP, "r");
        if(file_cputemp) {
                fscanf(file_cputemp, "%d", &cpu_temp);
                fclose(file_cputemp);
        }
}

float GDevice::GetBatteryVoltage()
{
        if(IsEmulator())
                return 3.8f;
        
        if(IsProduct(PRODUCT_X5001))
                return static_cast<float>(battery_voltage) / 1000;
                             
        if(IsProduct(PRODUCT_P880))
                return static_cast<float>(battery_voltage) / 1000 / 1000;
        
        return 0;
}
float GDevice::GetBatteryTemp()
{
        return IsEmulator() ? 20.5f : static_cast<float>(battery_temp) / 1000;
}

int GDevice::GetBatteryCapacity()
{
        return IsEmulator() ? 100 : battery_capacity;
}

float GDevice::GetCpuTemp()
{
        return IsEmulator() ? 30 : static_cast<float>(cpu_temp) / 1000;
}

bool GDevice::IsProduct(char product)
{
        if (product == PRODUCT_EMULATOR) {
                char out[32];
                property_get("ro.kernel.qemu", out, NULL);
                
                is_emulator = true;
                
                if(strtol(out, 0, 10) == 1)
                        return true;
        }
        else if(product == PRODUCT_X5001) {
                char out[32];
                property_get("ro.product.device", out, NULL);
                
                if(!strcmp(out, "x5001") || !strcmp(out, "X5001"))
                        return true;
        }
        else if(product == PRODUCT_P880) {
                char out[32];
                property_get("ro.product.device", out, NULL);
                
                if(!strcmp(out, "p880") || !strcmp(out, "P880") ||
                   !strcmp(out, "x3") || !strcmp(out, "X3"))
                        return true;
        }

        return false;
}

bool GDevice::IsEmulator()
{
        is_emulator = IsProduct(PRODUCT_EMULATOR);
        return is_emulator;
}

