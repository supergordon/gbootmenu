/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include "install.h"
#include "minzip/SysUtil.h"
#include "minzip/Zip.h"

#define ASSUMED_UPDATE_BINARY_NAME  "META-INF/com/google/android/update-binary"

        // Default allocation of progress bar segments to operations
static const int VERIFICATION_PROGRESS_TIME = 60;
static const float VERIFICATION_PROGRESS_FRACTION = 0.25;
static const float DEFAULT_FILES_PROGRESS_FRACTION = 0.4;
static const float DEFAULT_IMAGE_PROGRESS_FRACTION = 0.1;

static int
try_update_binary(const char *path, ZipArchive *zip, int* wipe_cache) {
        const ZipEntry* binary_entry =
        mzFindZipEntry(zip, ASSUMED_UPDATE_BINARY_NAME);
        if (binary_entry == NULL) {
                mzCloseZipArchive(zip);
                return INSTALL_CORRUPT;
        }
        
        const char* binary = "/tmp/update_binary";
        unlink(binary);
        int fd = creat(binary, 0755);
        if (fd < 0) {
                mzCloseZipArchive(zip);
                return INSTALL_ERROR;
        }
        bool ok = mzExtractZipEntryToFile(zip, binary_entry, fd);
        close(fd);
        mzCloseZipArchive(zip);
        
        if (!ok) {
                return INSTALL_ERROR;
        }
        
        int pipefd[2];
        pipe(pipefd);
        
        const char** args = (const char**)malloc(sizeof(char*) * 5);
        args[0] = binary;
        args[1] = strdup("3");   // defined in Android.mk
        char* temp = (char*)malloc(10);
        sprintf(temp, "%d", pipefd[1]);
        args[2] = temp;
        args[3] = (char*)path;
        args[4] = NULL;
        
        pid_t pid = fork();
        if (pid == 0) {
                close(pipefd[0]);
                execv(binary, (char* const*)args);
                fprintf(stdout, "E:Can't run %s (%s)\n", binary, strerror(errno));
                _exit(-1);
        }
        close(pipefd[1]);
        
        *wipe_cache = 0;
        
        char buffer[1024];
        FILE* from_child = fdopen(pipefd[0], "r");
        while (fgets(buffer, sizeof(buffer), from_child) != NULL) {
                char* command = strtok(buffer, " \n");
                if (command == NULL) {
                        continue;
                } else if (strcmp(command, "progress") == 0) {
                        char* fraction_s = strtok(NULL, " \n");
                        char* seconds_s = strtok(NULL, " \n");
                        
                        float fraction = strtof(fraction_s, NULL);
                        int seconds = strtol(seconds_s, NULL, 10);
                        
                } else if (strcmp(command, "set_progress") == 0) {
                        char* fraction_s = strtok(NULL, " \n");
                        float fraction = strtof(fraction_s, NULL);
                                //ui->SetProgress(fraction);
                } else if (strcmp(command, "ui_print") == 0) {
                        char* str = strtok(NULL, "\n");
                        if (str) {
                                        //  ui->Print("%s", str);
                        } else {
                                        //ui->Print("\n");
                        }
                } else if (strcmp(command, "wipe_cache") == 0) {
                        *wipe_cache = 1;
                }
        }
        fclose(from_child);
        
        int status;
        waitpid(pid, &status, 0);
        if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
                return INSTALL_ERROR;
        }
        
        return INSTALL_SUCCESS;
}

static int
really_install_package(const char *path, int* wipe_cache)
{
        ZipArchive zip;
        int err = mzOpenZipArchive(path, &zip);
        if (err != 0) {
                return INSTALL_CORRUPT;
        }
        return try_update_binary(path, &zip, wipe_cache);
}

int
install_package(const char* path, int* wipe_cache, const char* install_file)
{
        int result = really_install_package(path, wipe_cache);
        return result;
}
