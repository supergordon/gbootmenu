#ifndef __GCONDITION__
#define __GCONDITION__

#include "GSDK.h"
/*
 type_next_link:
        when GCondition::Item succeed the first item condition
        (*checkvalue == *check_against),
        then specify how the result should be linked to the next
        item condition.
 
        Possible link type:
                LINK_AND
                LINK_OR
                LINK_NOT
 
 linked_items:
        specifies the items which should be used to logical compare the item
        result with the current item result
 */
class GCondition {
public:
        enum Logic_Operators {
                NONE,
                AND,
                OR,
                EQUAL,
                UNEQUAL,
                LESS,
                GREATER
        };
private:
        struct Item {
                int id;
                int c1;
                int c2;
                int c3;
                Logic_Operators logic_operation;
                int linked_item;
        };
        
        Item item[16];
        int item_count;
        int cur_item;
public:
        GCondition();
        ~GCondition();
        
        static const int DONTCHECK = 0xFFFF;
        
        void Add(int c1, int c2 = DONTCHECK, int c3 = DONTCHECK);
        bool Check(int c1, int c2 = DONTCHECK, int c3 = DONTCHECK);
        void Reset();
        //void Add(int id, Logic_Operators op);
        //void Link(int item_id1, int item_id2);
};

#endif
