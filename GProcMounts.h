#ifndef __GPROCMOUNTS__
#define __GPROCMOUNTS__

#include "GSDK.h"

class GProcMounts : public GFileSystemTable {
public:
        class GProcMountsTable : public GFileSystemTable::GFileSystemTableContent {
        public:
                GProcMountsTable() {
                        dump = 0;
                        order = 0;
                }
                
                char dump;
                char order;
        };
private:
        char *filepath;
        std::vector<GProcMountsTable*> vpmt;
        std::vector<GProcMountsTable*>::iterator vpmt_it;
        
        GProcMounts::GProcMountsTable *ConvertLine(const char* line);
public:
        GProcMounts();
        ~GProcMounts();
        
        int ReadFromFile(const char* path = 0);
        int Add(GProcMountsTable* pmt);
        int GetCount();
        GProcMountsTable *Get(int id);
        int Refresh();
};

extern GProcMounts *g_pProcMounts;

#endif