#include "GSDK.h"

GSingleTouch *g_pSingleTouch = new GSingleTouch();
GMultiTouch *g_pMultiTouch = new GMultiTouch();


GSingleTouch::GSingleTouch()
{
        st = new touch_point;
}

GSingleTouch::~GSingleTouch()
{
        delete st;
}

bool GSingleTouch::EventHandlePointer(EventHandleInput *ev)
{
        if (ev->IsAbs()) {
                
                int code = ev->GetCode();
                int value = ev->GetValue();
                
                if (code == ABS_X || code == ABS_Y) {
                        OnEventPointerMove(code, value);
                        return true;
                }
        }
        return false;
}

/*
 on tap BTN_TOUCH gets raised and provides a state:
 1 => pressing
 0 => not pressing _anymore_
 */
bool GSingleTouch::EventHandleTap(EventHandleInput *ev)
{
        if (ev->IsKey()) {
                if (ev->GetCode() == BTN_TOUCH) {
                        if (ev->GetValue() == 1)
                                OnEventTapDown();
                        else
                                OnEventTapUp();
                        return true;
                }
        }
        return false;
}

void GSingleTouch::OnEventPointerMove(int code, int value)
{
        if (code == ABS_X)
                st->x = value;
        else if (code == ABS_Y)
                st->y = value;
}

void GSingleTouch::OnEventTapDown()
{
        //  g_pLog->Screen("[OnEvent] Tap down");
        st->ispressing = true;
}

void GSingleTouch::OnEventTapUp()
{
        //  g_pLog->Screen("[OnEvent] Tap up");
        st->ispressing = false;
}

bool GSingleTouch::IsNormalTap(int timeout)
{
        static int time_tapdown = 0;
        
        if (st->ispressing) {
                if (time_tapdown == 0)
                        time_tapdown = GTimer::GetTickCount();
        }
        else {
                if (time_tapdown) {
                        int diff = GTimer::GetTickCount() - time_tapdown;
                        time_tapdown = 0;
                        
                        if (timeout == 0 || (timeout && diff < timeout))
                                return true;
                }
        }
        return false;
}

bool GSingleTouch::IsDoubleTap()
{
        static int tap_count = 0;
        static int tap_starttime = 0;
        
        if(IsNormalTap()) {
                if(tap_count && (GTimer::GetTickCount() - tap_starttime) > 500) {
                        tap_count = 0;
                }
                
                tap_count++;
                
                if(tap_count == 1)
                        tap_starttime = GTimer::GetTickCount();
                
                if(tap_count == 2) {
                        tap_starttime = 0;
                        tap_count = 0;
                        
                        return true;
                }
        }
        return false;
}

bool GSingleTouch::IsHolding()
{
        return st->ispressing;
}

GMultiTouch::GMultiTouch()
{
        virtual_mt = new touch_point;
        real_mt = new touch_point;
        
        condRelease.Add(EV_SYN, SYN_MT_REPORT);
        condRelease.Add(EV_SYN, SYN_REPORT);
        condRelease.Add(EV_SYN, SYN_MT_REPORT);
        condRelease.Add(EV_SYN, SYN_REPORT);
}

GMultiTouch::~GMultiTouch()
{
        delete virtual_mt;
        delete real_mt;
}

void GMultiTouch::VirtualToScreen(touch_point *virt, touch_point *real)
{
        real->x = virt->x *
        g_pGraphics->GetWidth() / g_pGraphics->GetVirtualWidth();
        
        real->y = virt->y *
        g_pGraphics->GetHeight() / g_pGraphics->GetVirtualHeight();
        
        real->ispressing = virt->ispressing;
        real->isdragging = virt->isdragging;
        real->velocity = virt->velocity;
        real->size = virt->size;
}

bool GMultiTouch::EventHandlePointer(EventHandleInput *ev)
{
        if (ev->IsAbs()) {
                
                int code = ev->GetCode();
                int value = ev->GetValue();
                
                if (code == ABS_MT_POSITION_X || code == ABS_MT_POSITION_Y) {
                        OnEventPointerMove(code, value);
                        return true;
                }
        }
        return false;
}

void GMultiTouch::OnEventPointerMove(int code, int value)
{
        if (code == ABS_MT_POSITION_X)
                virtual_mt->x = value;
        else if (code == ABS_MT_POSITION_Y)
                virtual_mt->y = value;
        
        virtual_mt->ispressing = true;
        
        VirtualToScreen(virtual_mt, real_mt);
}

bool GMultiTouch::EventHandleTap(EventHandleInput *ev)
{
        if (condRelease.Check(ev->GetType(), ev->GetCode(), ev->GetValue())) {
                condRelease.Reset();
                virtual_mt->ispressing = false;
                real_mt->ispressing = false;
        }
               
        return true;
}

bool GMultiTouch::IsNormalTap(int timeout)
{
        static int time_tapdown = 0;
        
        if (IsHolding()) {
                if (time_tapdown == 0)
                        time_tapdown = GTimer::GetTickCount();
        }
        else {
                if (time_tapdown) { 
                        int diff = GTimer::GetTickCount() - time_tapdown;
                        time_tapdown = 0;
                        
                        if (timeout == 0 || (timeout && diff < timeout))
                                return true;
                }
        }
        return false;
}

bool GMultiTouch::IsHolding()
{
        return real_mt->ispressing;
}
