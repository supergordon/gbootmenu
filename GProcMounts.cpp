#include "GSDK.h"

GProcMounts *g_pProcMounts = new GProcMounts();

GProcMounts::GProcMounts()
{
        filepath = new char[256];
}

GProcMounts::~GProcMounts()
{
        delete [] filepath;
        
        for (vpmt_it = vpmt.begin(); vpmt_it != vpmt.end(); vpmt_it++)
                delete (*vpmt_it);
        
        vpmt.clear();
}

int GProcMounts::ReadFromFile(const char *path)
{
        FILE *file = fopen(path ? path : filepath, "r");
        
        if (path)
                strcpy(filepath, path);
        
        if (!file)
                return 1;

        do {
                char *buf = new char[256];
                memset(buf, 0, 256);
                
                if (!fgets(buf, 256, file))
                        continue;
                
                if (!GFileSystemTable::ValidateLine(buf))
                        continue;
                
                Add(ConvertLine(buf));
                
                delete [] buf;
        } while (!feof(file));
        
        fclose(file);
        
        return 0;
}

int GProcMounts::Add(GProcMountsTable *pmt)
{
        vpmt.push_back(pmt);
        return 0;
}

GProcMounts::GProcMountsTable *GProcMounts::ConvertLine(const char* line)
{
        GProcMountsTable *pmt = new GProcMountsTable();
        
        int i = 0;
        char *tok = strtok(const_cast<char*>(line), " \r\n\t");
        
        do {
                if (i == 0 && tok)
                        strcpy(pmt->device, tok);
                if (i == 1 && tok)
                        strcpy(pmt->mountpoint, tok);
                if (i == 2 && tok)
                        pmt->fstype = GFileSystemTable::ConvertType(tok);
                if (i == 3 && tok)
                        strcpy(pmt->options, tok);
                if (i == 4 && tok)
                        pmt->dump = static_cast<char>(strtol(tok, 0, 10));
                if (i == 5 && tok)
                        pmt->order = static_cast<char>(strtol(tok, 0, 10));
                if (i == 6)
                        break;
                
                i++;
        } while ((tok = strtok(0, " \r\n\t")));
        
        return pmt;
}

int GProcMounts::GetCount()
{
        return static_cast<int>(vpmt.size());
}

GProcMounts::GProcMountsTable *GProcMounts::Get(int id)
{
        if (id > GetCount())
                return 0;
        
        return vpmt.at(id);
}

int GProcMounts::Refresh()
{        
        for (vpmt_it = vpmt.begin(); vpmt_it != vpmt.end(); vpmt_it++)
                delete (*vpmt_it);
        
        vpmt.clear();
        ReadFromFile();
        
        return 0;
}