#ifndef __GSDK__
#define __GSDK__

const char* const GVER_STR = "GBootMenu 0.1.5";

extern "C" {
#include <pixelflinger/pixelflinger.h>
#include <unistd.h>
	
#include <linux/kd.h>
#include <linux/input.h>
#include <linux/fb.h>
        
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/mount.h>
#include <sys/wait.h>
#include <sys/reboot.h>
#include <sys/poll.h>
        
#include <cutils/properties.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdarg.h>
#include <dirent.h>
#include <png.h>
        
#include "install.h"
#include "minadbd/adb.h"
}

extern "C++" {
#include <vector>
#include <string>
        
#include "gui/GColor.h"
#include "gui/GSize.h"
#include "gui/GPoint.h"
#include "gui/GWindow.h"
#include "gui/GControl.h"
#include "gui/GPanel.h"
#include "gui/GGui.h"
  
#include "GDraw.h"
#include "GDevice.h"
#include "GTimer.h"
#include "GCondition.h"
#include "GBootMenu.h"
#include "GMain.h"
#include "GMenu.h"
#include "GGraphics.h"
#include "GLog.h"
#include "GFileSystemTable.h"
#include "GProcMounts.h"
#include "GFileTools.h"
#include "GVolume.h"
#include "GBootloader.h"
#include "GInput.h"
#include "GTouchInputHandler.h"
}

/*
TODO:
        battery stats
        cpu load + frequency + temp + percentage (for all cores + shadow core)
        gpu stats same like cpu above
        cpu + gpu regulating (clocking to a specific frequency)
        lcd backlight control
        button backlight control
 
        (char* => string)?
        touchscreen input
        multitouch support (ABS_MT_SLOT; see multitouch doc)
        !!save event output value and return with Getter methode by request!!
*/


/*
 https://www.kernel.org/doc/Documentation/
 */

#endif