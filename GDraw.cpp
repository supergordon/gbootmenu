#include "GSDK.h"

GDraw *g_pDraw = new GDraw();

GDraw::GDraw()
{
        
}

void GDraw::SetContext(GGLContext *ggl)
{
	gl = ggl;
}

void GDraw::Line(int ax, int ay, int bx, int by, int width, const GColor &colors)
{
	SetColor(colors);
        
        GGLcoord from[2] = {ax, ay};
        GGLcoord to[2] = {bx, by};
        
	gl->disable(gl, GGL_TEXTURE_2D);
  	gl->linex(gl, from, to, width);
	gl->enable(gl, GGL_TEXTURE_2D);
}

void GDraw::Quad(int x, int y, int w, int h, int width, const GColor &colors)
{
	FilledQuad(x, y, w, width, colors);
	FilledQuad(x, y+h, w, width, colors);
	FilledQuad(x, y, width, h, colors);
	FilledQuad(x+w, y, width, h+width, colors);
}

void GDraw::FilledQuad(int x, int y, int w, int h, const GColor &colors)
{
	SetColor(colors);
        
        gl->disable(gl, GGL_TEXTURE_2D);
        gl->recti(gl, x, y, x+w, y+h);
	gl->enable(gl, GGL_TEXTURE_2D);
}

void GDraw::Triangle(int ax, int ay, int bx, int by, int cx, int cy, const GColor &colors)
{
	SetColor(colors);
	
	GGLcoord v0[] = { ax, ay };
	GGLcoord v1[] = { bx, by };
	GGLcoord v2[] = { cx, cy };
	
	gl->disable(gl, GGL_TEXTURE_2D);
	gl->trianglex(gl, v0, v1, v2);
	gl->enable(gl, GGL_TEXTURE_2D);
}

void GDraw::Text(int x, int y, const GColor &colors, char alignment, const char *text, ...)
{
	char buf[128] = "";
	va_list args;
        va_start(args, text);
        vsnprintf(buf, sizeof(buf) - 1, text, args);
        va_end(args);
	
	if (alignment == ALIGN_CENTER)
		x -= g_pGraphics->GetTextSize(buf)/2;
        else if(alignment == ALIGN_RIGHT)
                x -= g_pGraphics->GetTextSize(buf);
	
	SetColor(colors);
	g_pGraphics->SetText(x, y, buf);
}

void GDraw::SetColor(const GColor &colors)
{
	GGLint glcolor[4];
	glcolor[0] = ((colors.r << 8) | colors.r) + 1;
        glcolor[1] = ((colors.g << 8) | colors.g) + 1;
        glcolor[2] = ((colors.b << 8) | colors.b) + 1;
        glcolor[3] = ((colors.a << 8) | colors.a) + 1;
        
        gl->color4xv(gl, glcolor);
}

int GDraw::GetFontHeight()
{
	int width = 0;
	int height = 0;
	
	g_pGraphics->GetFontSize(&width, &height);
	
	return height;
}
