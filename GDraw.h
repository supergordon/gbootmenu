#ifndef __GDRAW__
#define __GDRAW__

#include "GSDK.h"

enum {
        ALIGN_LEFT,
        ALIGN_CENTER,
        ALIGN_RIGHT
};

class GDraw {
private:
        GGLContext* gl;
        void SetColor(const GColor &colors);
public:
        GDraw();
        void SetContext(GGLContext* ggl);
        
        void Line(int ax, int ay, int bx, int by, int width, const GColor &colors);
        void Quad(int x, int y, int w, int h, int width, const GColor &colors);
        void FilledQuad(int x, int y, int w, int h, const GColor &colors);
        void Triangle(int ax, int ay, int bx, int by, int cx, int cy, const GColor &colors);
        void Text(int x, int y, const GColor &colors, char alignment, const char *text, ...);
        
        int GetFontHeight();
};

extern GDraw *g_pDraw;

#endif
