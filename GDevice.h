#ifndef __GDEVICE__
#define __GDEVICE__

#include "GSDK.h"

const char* const PATH_LCD_BRIGHTNESS =
"/sys/devices/platform/leds-mt65xx/leds/lcd-backlight/brightness";
//"/sys/devices/platform/tegra-i2c.1/i2c-1/1-0036/leds/lcd-backlight/brightness";

/*const char* const PATH_LCD_MAX_BRIGHTNESS =
"/sys/devices/platform/tegra-i2c.1/i2c-1/1-0036/leds/lcd-backlight/\
max_brightness";

const char* const PATH_LCD_BR_MAINTAIN_ON =
"/sys/devices/platform/tegra-i2c.1/i2c-1/1-0036/leds/lcd-backlight/\
br_maintain_on";*/

const char* const PATH_BAT_VOLTAGE =
"/sys/devices/platform/battery/power_supply/battery/batt_vol";
//"/sys/devices/platform/lge-battery/power_supply/battery/voltage_now";

const char* const PATH_BAT_TEMP =
"/sys/devices/virtual/thermal/thermal_zone3/temp";
//"/sys/devices/platform/battery/power_supply/battery/batt_temp";
//"/sys/devices/platform/lge-battery/power_supply/battery/temp";

const char* const PATH_BAT_CAPACITY =
"/sys/devices/platform/battery/power_supply/battery/capacity";
//"/sys/devices/platform/lge-battery/power_supply/battery/capacity";

const char *const PATH_CPU_TEMP =
"/sys/devices/virtual/thermal/thermal_zone1/temp";

enum {
        PRODUCT_INVALID,
        PRODUCT_EMULATOR,
        PRODUCT_P880,
        PRODUCT_CRESPO,
        PRODUCT_X5001,
};

enum {
        LED_NONE,
        LED_RED,
        LED_GREEN,
        LED_BLUE
};

class GDevice {
public:
        GDevice();
        void Init();
        int GetLcdBrightness();
        void SetLcdBrightness(int percentage);
        void SetLcdPowerstate(bool on);
        void UpdateStats();
        
        float GetBatteryVoltage();
        float GetBatteryTemp();
        int GetBatteryCapacity();
        float GetCpuTemp();
        
        bool IsProduct(char product);
        bool IsEmulator();
        
        void SetLed(int led);
private:
        int cur_brightness;
        int max_brightness;
        int old_brightness;
        
        int battery_voltage; //4253000µV -> 4253mV -> 4,253V
        int battery_temp; //257 / 10 = 25.7°C
        int battery_capacity; //0 - 100
        int cpu_temp;
        
        bool is_emulator;
protected:
};

extern GDevice *g_pDevice;

#endif
