#include "GSDK.h"

GFileSystemTable *g_pFileSystemTable = new GFileSystemTable();

GFileSystemTable::GFileSystemTable()
{
        
}

GFileSystemTable::~GFileSystemTable()
{
        for (vfst_it = vfst.begin(); vfst_it != vfst.end(); vfst_it++)
                delete (*vfst_it);
        
        vfst.clear();
}

int GFileSystemTable::ReadFromFile(const char *filepath)
{
        FILE *file = fopen(filepath, "r");
        
        if (!file)
                return 1;
        
        do {
                char *buf = new char[256];
                memset(buf, 0, 256);
                
                if (!fgets(buf, 256, file))
                        continue;
                
                if (!ValidateLine(buf))
                        continue;
                
                Add(ConvertLine(buf));
                
                delete [] buf;
        } while (!feof(file));
        
        fclose(file);
        
        return 0;
}

int GFileSystemTable::Add(GFileSystemTableContent *fstc)
{
        vfst.push_back(fstc);
        return 0;
}

bool GFileSystemTable::ValidateLine(const char *line, bool mtab)
{
        if (!line || strlen(line) < 10)
                return false;
        
        const char c = line[0];
        
        if (!mtab) {
                if (c == '\r' || c == '\n' || c != '/') {
                        return false;
                }
        }
        else {
                if (c == '\r' || c == '\n') {
                        return false;
                }
        }
        
        return true;
}

char GFileSystemTable::ConvertType(const char *fs)
{
        if (!fs)
                return NONE;
        
        if (strcmp(fs, "ext2") == 0) return EXT2;
        if (strcmp(fs, "ext3") == 0) return EXT3;
        if (strcmp(fs, "ext4") == 0) return EXT4;
        if (strcmp(fs, "emmc") == 0) return EMMC;
        if (strcmp(fs, "mtd") == 0) return MTD;
        if (strcmp(fs, "vfat") == 0) return VFAT;
        if (strcmp(fs, "exfat") == 0) return EXFAT;
        if (strcmp(fs, "datamedia") == 0) return DATAMEDIA;
        
        if (strcmp(fs, "fuse") == 0) return FUSE;
        if (strcmp(fs, "rootfs") == 0) return ROOTFS;
        if (strcmp(fs, "devpts") == 0) return DEVPTS;
        if (strcmp(fs, "proc") == 0) return PROC;
        if (strcmp(fs, "sysfs") == 0) return SYSFS;
        if (strcmp(fs, "debugfs") == 0) return DEBUGFS;
        if (strcmp(fs, "none") == 0) return NONE;
        
        return NONE;
}

char *GFileSystemTable::ConvertType(const char type)
{
        if (type == EXT2) return strdup("ext2");
        if (type == EXT3) return strdup("ext3");
        if (type == EXT4) return strdup("ext4");
        if (type == EMMC) return strdup("emmc");
        if (type == MTD) return strdup("mtd");
        if (type == VFAT) return strdup("vfat");
        if (type == EXFAT) return strdup("exfat");
        if (type == DATAMEDIA) return strdup("datamedia");
        
        if (type == FUSE) return strdup("fuse");
        if (type == ROOTFS) return strdup("rootfs");
        if (type == DEVPTS) return strdup("devpts");
        if (type == PROC) return strdup("proc");
        if (type == SYSFS) return strdup("sysfs");
        if (type == DEBUGFS) return strdup("debugfs");
        if (type == NONE) return strdup("none");
        
        return 0;
}

GFileSystemTable::GFileSystemTableContent
*GFileSystemTable::ConvertLine(const char *line)
{
        GFileSystemTableContent *fstc = new GFileSystemTableContent();
        
        int i = 0;
        char *tok = strtok(const_cast<char*>(line), " \r\n\t");
        
        do {
                if (i == 0 && tok)
                        strcpy(fstc->mountpoint, tok);
                if (i == 1 && tok)
                        fstc->fstype = ConvertType(tok);
                if (i == 2 && tok)
                        strcpy(fstc->device, tok);
                if (i == 3 && tok)
                        strcpy(tok[0] == '/' ? fstc->device2 : fstc->options, tok);
                if (i == 4)
                        break;
                
                i++;
        } while ((tok = strtok(0, " \r\n\t")));
        
        char *symlink = g_pFileTools->resolveSymlink(fstc->device);
        if (symlink) {
                strcpy(fstc->device, symlink);
                delete [] symlink;
        }
        
        return fstc;
}

int GFileSystemTable::GetCount()
{
        return static_cast<int>(vfst.size());
}

GFileSystemTable::GFileSystemTableContent *GFileSystemTable::Get(int id)
{
        if (id > GetCount())
                return 0;
        
        return vfst.at(id);
}