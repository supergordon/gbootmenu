#include "GSDK.h"

GBootloader *g_pBootloader = new GBootloader;

void GBootloader::WriteCommand(const char *command)
{
        FILE* file = fopen(g_pVolume->Get("/misc")->device, "wb");
        
        if (!file)
                return;

        GBootloader::GMessage msg;
        
        if(command)
                strcpy(msg.command, command);
        
        fwrite(&msg, sizeof(GBootloader::GMessage), 1, file);
        fclose(file);
}

void GBootloader::GetMessage(GMessage &msg)
{
        FILE* file = fopen(g_pVolume->Get("/misc")->device, "rb");
        
        if (!file)
                return;
        
        fread(&msg, sizeof(GBootloader::GMessage), 1, file);
        fclose(file);
}

void GBootloader::Clear()
{
        WriteCommand(NULL);
}

void GBootloader::Reboot(const char type, const char *arg)
{
        if (type == RESTART2) {
                __reboot(LINUX_REBOOT_MAGIC1, LINUX_REBOOT_MAGIC2,
                         LINUX_REBOOT_CMD_RESTART2, (char*)arg);
        }
        else if (type == POWEROFF) {
                reboot(RB_POWER_OFF);
        }
        else if (type == RESTART) {
                reboot(RB_AUTOBOOT);
        }
}