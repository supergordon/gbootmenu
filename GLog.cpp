#include "GSDK.h"

GLog *g_pLog = new GLog();

#define __LOG__

GLog::GLog()
{
        
}

GLog::~GLog()
{
        for (v_it = v.begin(); v_it != v.end(); v_it++)
                delete [] *v_it;
        
        v.clear();
}

void GLog::GetTime(char *out)
{
        timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);
        
        time_t time_cur = static_cast<time_t>(ts.tv_sec);
        tm *tm_cur = localtime(&time_cur);
        tm_cur->tm_hour += 1;
        
        char buf_time[32];
        strftime(buf_time, 32, "%H:%M:%S", tm_cur);
        
        sprintf(out, "%s.%03d", buf_time, static_cast<int>(ts.tv_nsec/1000/1000));
}

void GLog::Console(const char *text, ...)
{
#ifdef __LOG__
        char buf[128] = "";
        va_list args;
    	va_start(args, text);
    	vsnprintf(buf, sizeof(buf) - 1, text, args);
    	va_end(args);
	
        this->File(buf);
        
        char buf_time[32];
        GetTime(buf_time);
        
        printf("(%s) %s", buf_time, buf);
#endif
}

void GLog::Screen(const char *text, ...)
{
#ifdef __LOG__
	char buf[128] = "";
	va_list args;
        va_start(args, text);
        vsnprintf(buf, sizeof(buf) - 1, text, args);
        va_end(args);
        
        if (v.size() > 8) {
                delete [] *v.begin();
                v.erase(v.begin());
        }
        
        char buf_time[32];
        GetTime(buf_time);
        
        char out[128] = "";
        sprintf(out, "(%s) %s", buf_time, buf);
        
	v.push_back(strdup(out));
        g_pGraphicsUpdater->Request();
#endif
}

void GLog::ScreenRender()
{
#ifdef __LOG__
        int c = v.size();
        
        int fontheight = g_pDraw->GetFontHeight();
        int boxwidth = g_pGraphics->GetWidth() - 20;
        int boxheight = fontheight * c + 10;
        
        int x = 10;
        int y = g_pGraphics->GetHeight() - boxheight - 10;
        
        g_pDraw->FilledQuad(x, y, boxwidth, boxheight, color_grey);
        g_pDraw->Quad(x, y, boxwidth, boxheight, 1, GColor(170, 170, 170));
	
	for (int i = 0; i < c; i++)
                g_pDraw->Text(x+10, y+fontheight+(fontheight*i)+5,
                              color_black, ALIGN_LEFT, v.at(i));
#endif
}

void GLog::File(const char *text, ...)
{
#ifdef __LOG__
	char buf[128] = "";
	va_list args;
        va_start(args, text);
        vsnprintf(buf, sizeof(buf) - 1, text, args);
        va_end(args);
	
	FILE *file = fopen("/cache/bootmenu.log", "a+");
	
	if (!file) {
		this->Screen("[GLog] Unable to open file...");
		return;
	}
        
        char buf_time[32];
        GetTime(buf_time);
        
        char out[128] = "";
        sprintf(out, "(%s) %s", buf_time, buf);
	
	fprintf(file, "%s\n", out);
	fclose(file);
#endif
}