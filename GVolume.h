#ifndef __GVOLUME__
#define __GVOLUME__

#include "GSDK.h"

class GVolume {
private:
public:
        class GVolumes : public GFileSystemTable::GFileSystemTableContent {
                
        };
        
        GVolume();
        ~GVolume();
        
        bool IsMounted(const char *path);
        GVolumes *Get(const char *path);
        bool Mount(const char *path);
        bool Unmount(const char *path, bool force = false);
        bool UnmountAll();
};

extern GVolume *g_pVolume;

#endif