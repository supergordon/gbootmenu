#ifndef __GBOOTLOADER__
#define __GBOOTLOADER__

#include "GSDK.h"

class GBootloader {
public:
        enum GReboot {
                RESTART,
                POWEROFF,
                RESTART2
        };
        
        struct GMessage {
                char command[32];
                char status[32];
                char recovery[1024];
        };
        
        void WriteCommand(const char *command);
        void Clear();
        void GetMessage(GMessage &msg);
        void Reboot(const char type, const char *arg = 0);
};

extern GBootloader *g_pBootloader;

#endif
