#include "GSDK.h"

GCondition::GCondition()
{
        item_count = 0;
        cur_item = 0;
}

GCondition::~GCondition()
{
        
}

void GCondition::Add(int c1, int c2, int c3)
{
        Item *i = &item[item_count];
        
        i->id = item_count++;
        i->c1 = c1;
        i->c2 = c2;
        i->c3 = c3;
        
}

bool GCondition::Check(int c1, int c2, int c3)
{
        
        Item *i = &item[cur_item];
        
        if (c1 == i->c1 &&
            (i->c2 == DONTCHECK || i->c2 == c2) &&
            (i->c3 == DONTCHECK || i->c3 == c3)) {
                
                cur_item++;
                if (cur_item == item_count) {
                        return true;
                }
        }
        else {
                Reset();
        }
        
        return false;
}

void GCondition::Reset()
{
        cur_item = 0;
}

/*
void GCondition::Add(int id, GCondition::Logic_Operators op)
{
        Item *i = &item[id];
        
        i->logic_operation = op;
}

void GCondition::Link(int item_id1, int item_id2)
{
        Item *i1 = &item[item_id1];
        Item *i2 = &item[item_id2];
        
        i1->linked_item = i2->id;
}*/