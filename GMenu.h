#ifndef __GMENU__
#define __GMENU__

#include "GSDK.h"

class GMenuElement {
public:
	GMenuElement()
        {
		this->value = new bool;
		*this->value = false;
	}
	GMenuElement(const char *entry, const char *desc = 0)
        {
		this->value = new bool;
		*this->value = false;
		this->entry = strdup(entry);
		
                if (desc)
                        this->desc = strdup(desc);
	}
        ~GMenuElement()
        {
                if (entry)
                        delete [] entry;
                if (desc)
                        delete [] desc;
                
                delete value;
        }
	char *entry;
	char *desc;
	bool *value;
};

class GMenu {
private:
	char *title;
	int s;
	int x, y;
	int w;
	bool bOnHighlighting;
public:
	void Init();
	~GMenu();
        
	void AddEntry(GMenuElement *item);
	bool KeyHandling(int key);
	void Draw();
        
        std::vector<GMenuElement*> v;
        std::vector<GMenuElement*>::iterator v_it;
        
	enum {
		GVK_VOLUP,
		GVK_VOLDOWN,
		GVK_POWER,
		GVK_BACK,
		GVK_HOME,
		GVK_MENU
	};
};

extern GMenu *g_pMenu;

extern GMenuElement *g_pCWM;
extern GMenuElement *g_pTWRP;
extern GMenuElement *g_pStockRecovery;
extern GMenuElement *g_pAromafm;
extern GMenuElement *g_pFastboot;
extern GMenuElement *g_pReboot;

#endif
