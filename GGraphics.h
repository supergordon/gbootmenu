#ifndef __GGRAPHICS__
#define __GGRAPHICS__

#include "GSDK.h"

typedef struct {
        GGLSurface texture;
        unsigned int cwidth;
        unsigned int cheight;
        unsigned int ascent;
} GRFont;

class GGraphicsUpdater {
public:
        GGraphicsUpdater() {
                count = 0;
        }
        void Request() {
                count++;
        }
        bool NeedsRender() {
                return count > 0;
        }
        void Clear() {
                count = 0;
        }
private:
        int count;
};

extern GGraphicsUpdater *g_pGraphicsUpdater;

class GGraphics {
private:
	GGLContext *gl;
        bool updateScreen;
public:
	void Init();
        void Destroy();
        
	GGLContext* GetContext();
        
	int GetWidth() const;
	int GetHeight() const;
        
        int GetVirtualWidth() const;
	int GetVirtualHeight() const;

        static void *RenderThread(void *this_ptr);
        
        int GetFramebuffer(GGLSurface *fb);
        void GetMemorySurface(GGLSurface* ms);
        void SetActiveFramebuffer(unsigned int n);
        void Blank(bool blank);
        int InitGraphics();
        void Exit();
        void InitFont();
        void Flip();
        void GetFontSize(int *x, int *y);
        int GetTextSize(const char *s);
        int SetText(int x, int y, const char *s);
        
        int fb_fd;
        int vt_fd;
        
        fb_var_screeninfo vi;
        fb_fix_screeninfo fi;
        
        GRFont *font;
        GGLSurface font_texture;
        GGLSurface framebuffer[2];
        GGLSurface mem_surface;
        unsigned int active_fb;
        unsigned int double_buffering;
};

extern GGraphics *g_pGraphics;

#endif
