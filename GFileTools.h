#ifndef __GFILETOOLS__
#define __GFILETOOLS__

class GFileTools {
private:
public:
        bool isPath(const char* path);
        bool isSymlink(const char* path);
        bool isDirectory(const char* path);
        bool isRegular(const char* path);
        bool isBlock(const char* path);
        bool isCharacter(const char* path);
        
        char *resolveSymlink(const char* path);
        
        unsigned long long getFileSize(const char* path);
        unsigned long long getFolderSize(const char* path);
};

extern GFileTools *g_pFileTools;

#endif
