#!/sbin/sh

if [ $1 = "clockworkmod" ]
then
	rm /sbin/recovery
	unzip -o -q /tmp/clockworkmod.zip -d /
	chmod +x /sbin/*
    kill $(ps | grep /sbin/virtual_sd)
	kill $(ps | grep /sbin/recovery)

elif [ $1 = "twrp" ]
then
	rm /sbin/recovery
	unzip -o -q /tmp/twrp.zip -d /
	chmod +x /sbin/*
# kill $(ps | grep /sbin/virtual_sd)
	kill $(ps | grep /sbin/recovery)

elif [ $1 = "stock_recovery" ]
then
	rm -f /sbin/recovery
	unzip -o -q /tmp/stock_recovery.zip -d /
	chmod +x /sbin/*
	kill $(ps | grep /sbin/recovery)

elif [ $1 = "aromafm" ]
then
	unzip -opq /tmp/aromafm.zip META-INF/com/google/android/update-binary > /tmp/update_binary
	chmod +x /tmp/*
fi