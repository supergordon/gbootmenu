#include "GSDK.h"

GInput *g_pInput = new GInput();

GInput::GInput()
{
        event_input_count = 0;
        
        ta = static_cast<touch_abs**>(malloc(sizeof(int*) * 2));
        ta[0] = new touch_abs;
        ta[1] = new touch_abs;
        
        event_input = static_cast<EventInput**>(malloc(sizeof(int*) *
                                                       MAX_EVENT_INPUT_COUNT));
}

GInput::~GInput()
{
        delete ta[0];
        delete ta[1];
        free(ta);
        
        for (int i = 0; i < event_input_count; i++)
                delete event_input[i];
        
        free(event_input);
}

void GInput::Init()
{
        EventInit();
        
        pthread_t t;
        pthread_create(&t, NULL, EventInputThread, this);
}

int GInput::GetVirtualWidth()
{
	return ta[0]->maximum;
}

int GInput::GetVirtualHeight()
{
	return ta[1]->maximum;
}

void GInput::EventProcessInput(EventHandleInput *ev)
{
        //  g_pLog->Screen("Z%u | [T%08X] (C%08X) {V%08X}",
        //             ev->GetTime()->tv_sec*1000+ev->GetTime()->tv_usec/1000,
        //            ev->GetType(),ev->GetCode(), ev->GetValue());
        
        
        if (ev->IsKey()) {
                if (ev->IsKeyCode(KEY_VOLUMEUP))
                        g_pMenu->KeyHandling(GMenu::GVK_VOLUP);
                if (ev->IsKeyCode(KEY_VOLUMEDOWN))
                        g_pMenu->KeyHandling(GMenu::GVK_VOLDOWN);
                if (ev->IsKeyCode(KEY_POWER))
                        g_pMenu->KeyHandling(GMenu::GVK_POWER);
                if (ev->IsKeyCode(KEY_BACK))
                        g_pMenu->KeyHandling(GMenu::GVK_BACK);
                if (ev->IsKeyCode(KEY_HOME))
                        g_pMenu->KeyHandling(GMenu::GVK_HOME);
                if (ev->IsKeyCode(KEY_MENU))
                        g_pMenu->KeyHandling(GMenu::GVK_MENU);
        }
        if (ev->IsAbs()) {
               
        }
        if (ev->IsSyn()) {
                
        }

        g_pGraphicsUpdater->Request();
        
        if(g_pDevice->IsEmulator()) {
                g_pSingleTouch->EventHandlePointer(ev);
                g_pSingleTouch->EventHandleTap(ev);
        }
        else {
                g_pMultiTouch->EventHandlePointer(ev);
                g_pMultiTouch->EventHandleTap(ev);
        }
        
        g_pGraphicsUpdater->Request();
}
/*
 #define SYN_REPORT		0
 #define SYN_CONFIG		1
 #define SYN_MT_REPORT		2
 #define SYN_DROPPED		3
 #define ABS_MT_SLOT		0x2f
 #define ABS_MT_TOUCH_MAJOR	0x30
 #define ABS_MT_TOUCH_MINOR	0x31
 #define ABS_MT_WIDTH_MAJOR	0x32
 #define ABS_MT_WIDTH_MINOR	0x33
 #define ABS_MT_ORIENTATION	0x34
 #define ABS_MT_POSITION_X	0x35
 #define ABS_MT_POSITION_Y	0x36
 #define ABS_MT_TOOL_TYPE	0x37
 #define ABS_MT_BLOB_ID		0x38
 #define ABS_MT_TRACKING_ID	0x39
 #define ABS_MT_PRESSURE	0x3a
 #define ABS_MT_DISTANCE	0x3b
 
 
 ABS_MT_TRACKING_ID 0
 SYN_MT_REPORT 0
 SYN_REPORT 0
 ABS_MT_POSITION_X
 ABS_MT_POSITION_Y
 ABS_MT_PRESSURE
 ABS_MT_WIDTH_MAJOR 5
 ABS_MT_WIDTH_MINOR 3
 ABS_MT_ORIENTATION 0
 */

/* timeout has the same semantics as for poll
 *    0 : don't block
 *  < 0 : block forever
 *  > 0 : block for 'timeout' milliseconds
 */
int GInput::EventWait(int timeout)
{
        if (poll(event_input_poll, event_input_count, timeout) > 0)
                return 1;
        
        return 0;
}

void *GInput::EventInputThread(void *this_ptr)
{
        pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
        
        GInput *pi = static_cast<GInput*>(this_ptr);
        
        do {
                if (!pi->EventWait(-1))
                        continue;
                
                for (int i = 0; i < pi->event_input_count; i++) {
                        if (!(pi->event_input_poll[i].revents & POLLIN))
                                continue;
                        
                        EventInput *ev = new EventInput();
                        
                        g_pGraphicsUpdater->Request();
                        
                        if (read(pi->event_input_poll[i].fd, ev, sizeof(*ev))
                            == sizeof(*ev)) {
                                g_pGraphicsUpdater->Request();
                                pthread_mutex_lock(&mutex);
                                pi->EventProcessInput(static_cast<EventHandleInput*>(ev));
                                pthread_mutex_unlock(&mutex);
                        }
                        delete ev;
                }
        } while (true);
        
        return NULL;
}

int GInput::EventInit()
{
        dirent *de;
        DIR *dir = opendir("/dev/input");
        
        if (dir == NULL)
                return 0;
        
        while ((de = readdir(dir))) {
                if (strncmp(de->d_name,"event", 5))
                        continue;
                
                int fd = openat(dirfd(dir), de->d_name, O_RDONLY);
                if (fd == -1)
                        continue;
                
                int c = event_input_count;
                event_input_poll[c].fd = fd;
                event_input_poll[c].events = POLLIN;
                
                event_input_count++;
                
                if (c == 1) { //event1
                        ioctl(fd, EVIOCGABS(ABS_MT_POSITION_X), ta[0]);
                        ioctl(fd, EVIOCGABS(ABS_MT_POSITION_Y), ta[1]);
                }
                
                if (event_input_count == 16)
                        break;
        }
        return 0;
}

/*
 Notes for multitouch:
 
 For type B devices, the kernel driver should associate a slot with each
 identified contact, and use that slot to propagate changes for the contact.
 Creation, replacement and destruction of contacts is achieved by modifying
 the ABS_MT_TRACKING_ID of the associated slot.  A non-negative tracking id
 is interpreted as a contact, and the value -1 denotes an unused slot.  A
 tracking id not previously present is considered new, and a tracking id no
 longer present is considered removed.  Since only changes are propagated,
 the full state of each initiated contact has to reside in the receiving
 end.  Upon receiving an MT event, one simply updates the appropriate
 attribute of the current slot.
 
 Here is what a minimal event sequence for a two-contact touch would look
 like for a type B device:
 
 ABS_MT_SLOT 0
 ABS_MT_TRACKING_ID 45
 ABS_MT_POSITION_X x[0]
 ABS_MT_POSITION_Y y[0]
 ABS_MT_SLOT 1
 ABS_MT_TRACKING_ID 46
 ABS_MT_POSITION_X x[1]
 ABS_MT_POSITION_Y y[1]
 SYN_REPORT
 
 Here is the sequence after moving contact 45 in the x direction:
 
 ABS_MT_SLOT 0
 ABS_MT_POSITION_X x[0]
 SYN_REPORT
 
 Here is the sequence after lifting the contact in slot 0:
 
 ABS_MT_TRACKING_ID -1
 SYN_REPORT
 
 The slot being modified is already 0, so the ABS_MT_SLOT is omitted.  The
 message removes the association of slot 0 with contact 45, thereby
 destroying contact 45 and freeing slot 0 to be reused for another contact.
 
 Finally, here is the sequence after lifting the second contact:
 
 ABS_MT_SLOT 1
 ABS_MT_TRACKING_ID -1
 SYN_REPORT
 */

/*
 Logged Events
 
 Key press
 EV_KEY value is 1 => key down
 EV_KEY value is 0 => key up
 EV_KEY code xy is keycode
 
 [EV_KEY] (KEY_VOLUMEDOWN) {V00000001}
 [EV_SYN] (SYN_REPORT) {V00000000}
 [EV_KEY] (KEY_VOLUMEDOWN) {V00000000}
 [EV_SYN] (SYN_REPORT) {V00000000}
 
 
 One finger touch
 
 [EV_ABS] (ABS_MT_POSITION_X) {V000002CF}
 [EV_ABS] (ABS_MT_POSITION_Y) {V000004EC}
 [EV_ABS] (ABS_MT_PRESSURE) {V00000021}
 [EV_ABS] (ABS_MT_WIDTH_MAJOR) {V00000002}
 [EV_ABS] (ABS_MT_WIDTH_MINOR) {V00000002}
 [EV_ABS] (ABS_MT_ORIENTATION) {V00000001}
 [EV_ABS] (ABS_MT_TRACKING_ID) {V00000000}
 [EV_SYN] (SYN_MT_REPORT) {V00000000}
 [EV_SYN] (SYN_REPORT) {V00000000}
 
 
 Realising the finger generates an additional SYN event for all touch events
 
 [EV_SYN] (SYN_MT_REPORT) {V00000000}
 [EV_SYN] (SYN_REPORT) {V00000000}
 
 
 Two finger touch
 
 First finger (tracking id 0)
 [EV_ABS] (ABS_MT_POSITION_X) {V00000385}
 [EV_ABS] (ABS_MT_POSITION_Y) {V000004EB}
 [EV_ABS] (ABS_MT_PRESSURE) {V0000004D}
 [EV_ABS] (ABS_MT_WIDTH_MAJOR) {V00000005}
 [EV_ABS] (ABS_MT_WIDTH_MINOR) {V00000004}
 [EV_ABS] (ABS_MT_ORIENTATION) {V00000001}
 [EV_ABS] (ABS_MT_TRACKING_ID) {V00000000}
 [EV_SYN] (SYN_MT_REPORT) {V00000000}
 [EV_SYN] (SYN_REPORT) {V00000000}
 
 Second finger (tracking id 1 !no SYN_REPORT!)
 [EV_ABS] (ABS_MT_POSITION_X) {V0000017A}
 [EV_ABS] (ABS_MT_POSITION_Y) {V000005C2}
 [EV_ABS] (ABS_MT_PRESSURE) {V0000003E}
 [EV_ABS] (ABS_MT_WIDTH_MAJOR) {V00000004}
 [EV_ABS] (ABS_MT_WIDTH_MINOR) {V00000003}
 [EV_ABS] (ABS_MT_ORIENTATION) {V00000000}
 [EV_ABS] (ABS_MT_TRACKING_ID) {V00000001}
 [EV_SYN] (SYN_MT_REPORT) {V00000000}
 
 */