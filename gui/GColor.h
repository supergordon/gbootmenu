#ifndef __GCOLOR__
#define __GCOLOR__

class GColor {
public:
        GColor();
        GColor(unsigned char r, unsigned char g, unsigned char b,
               unsigned char a = 255);
        GColor(const char *colorName);
        GColor(const GColor &color);
        GColor &operator =(const GColor &color);
        unsigned int Get();
        
        unsigned char r, g, b, a;
private:
        unsigned int color;
        bool isok;
protected:
        void Set();
};

extern GColor color_red;
extern GColor color_green;
extern GColor color_blue;
extern GColor color_yellow;
extern GColor color_orange;
extern GColor color_white;
extern GColor color_black;
extern GColor color_grey;
extern GColor color_pink;

extern GColor color_lightblue;
extern GColor color_darkgreen;
extern GColor color_firebrick;
extern GColor color_deeppink;
extern GColor color_cadetblue;
extern GColor color_maroon;
extern GColor color_chocolate;

#endif