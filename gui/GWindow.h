#ifndef __GWINDOW__
#define __GWINDOW__

#include "GSDK.h"

class GWindow {
private:
public:
        GWindow();

        int x;
        int y;
        int w;
        int h;
        
        GColor background_color;
};

#endif
