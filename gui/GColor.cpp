#include "GColor.h"

GColor color_red(255, 0, 0);
GColor color_green(0, 255, 0);
GColor color_blue(0, 0, 255);
GColor color_yellow(255, 255, 0);
GColor color_orange(255, 100, 0);
GColor color_white(255, 255, 255);
GColor color_black(0, 0, 0);
GColor color_grey(139, 137, 137);
GColor color_pink(255, 0, 255);

GColor color_lightblue(30, 144, 255);
GColor color_darkgreen(0, 180, 0);
GColor color_firebrick(178, 34, 34);
GColor color_deeppink(255, 20, 147);
GColor color_cadetblue(152, 245, 255);
GColor color_maroon(139, 28, 98);
GColor color_chocolate(139, 69, 19);

GColor::GColor()
{
        r = 0;
        g = 0;
        b = 0;
        a = 0;
        
        isok = false;
}

GColor::GColor(unsigned char r, unsigned char g, unsigned char b,
               unsigned char a)
{
        
        this->r = r; this->g = g; this->b = b; this->a = a;
        Set();
}

GColor::GColor(const GColor &colors)
{
        *this = colors;
}

GColor &GColor::operator =(const GColor &colors)
{
        r = colors.r;
        g = colors.g;
        b = colors.b;
        a = colors.a;
        
        Set();
        
        return *this;
}

void GColor::Set()
{
        color = 0;
        
        color = a << 24;
        color = r << 16;
        color = g << 8;
        color = b << 0;
}

unsigned int GColor::Get()
{
        return color;
}