#ifndef __GSIZE__
#define __GSIZE__


class GSize {
private:
       
public:
        GSize();
        GSize(int width, int height);
        
        void operator =(const GSize &size);
        bool operator ==(const GSize &size);
        bool operator !=(const GSize &size);
        
        GSize operator +(const GSize &size);
        GSize operator -(const GSize &size);
        
        GSize &operator +=(const GSize &size);
        GSize &operator -=(const GSize &size);
        
        GSize operator /(float factor);
        GSize operator *(float factor);
        
        GSize &operator /=(float factor);
        GSize &operator *=(float factor);
        
        int width;
        int height;
        
        void DecBy(const GSize &size);
        void DecBy(int dx, int dy);
        void DecBy(int d);
        void DecTo(const GSize &size);
        bool IsFullySpecified() const;
        int GetWidth() const;
        int GetHeight() const;
        void IncBy(const GSize &size);
        void IncBy(int dx, int dy);
        void IncBy(int d);
        void IncTo(const GSize &size);
        void SetWidth(int width);
        void SetHeight(int height);
        void Set(int width, int height);
        GSize &Scale(float xscale, float yscale);
};

const GSize G_DefaultSize = GSize(-1, -1);

#endif
