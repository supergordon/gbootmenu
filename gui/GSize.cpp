#include "GSize.h"

GSize::GSize()
{
        width = -1;
        height = -1;
}

GSize::GSize(int width, int height)
{
        this->width = width;
        this->height = height;
}

void GSize::operator =(const GSize &size)
{
        width = size.width;
        height = size.height;
}

bool GSize::operator ==(const GSize &size)
{
        return width == size.width && height == size.height;
}

bool GSize::operator !=(const GSize &size)
{
        return width != size.width || height != size.height;
}

GSize GSize::operator +(const GSize &size)
{
        width += size.width;
        height += size.height;
        
        return *this;
}
GSize GSize::operator -(const GSize &size)
{
        width -= size.width;
        height -= size.height;
        
        return *this;
}

GSize &GSize::operator +=(const GSize &size)
{
        width += size.width;
        height += size.height;
        
        return *this;
}
GSize &GSize::operator -=(const GSize &size)
{
        width -= size.width;
        height -= size.height;
        
        return *this;
}

GSize GSize::operator /(float factor)
{
        width /= factor;
        height /= factor;
        
        return *this;
}
GSize GSize::operator *(float factor)
{
        width *= factor;
        height *= factor;
        
        return *this;
}

GSize &GSize::operator /=(float factor)
{
        width /= factor;
        height /= factor;
        
        return *this;
}
GSize &GSize::operator *=(float factor)
{
        width *= factor;
        height *= factor;
        
        return *this;
}

void GSize::DecBy(const GSize &size)
{
        width -= size.width;
        height -= size.height;
}

void GSize::DecBy(int dx, int dy)
{
        width -= dx;
        height -= dy;
}

void GSize::DecBy(int d)
{
        width -= d;
        height -= d;
}

void GSize::DecTo(const GSize &size)
{
        width = size.width;
        height = size.height;
}

bool GSize::IsFullySpecified() const
{
        return width != -1 && height != -1;
}

int GSize::GetWidth() const
{
        return width;
}

int GSize::GetHeight() const
{
        return height;
}

void GSize::IncBy(const GSize &size)
{
        width += size.width;
        height += size.height;
}

void GSize::IncBy(int dx, int dy)
{
        width += dx;
        height += dy;
}

void GSize::IncBy(int d)
{
        width += d;
        height += d;
}

void GSize::IncTo(const GSize &size)
{
        width = size.width;
        height = size.height;
}

void GSize::SetWidth(int width)
{
        this->width = width;
}

void GSize::SetHeight(int height)
{
        this->height = height;
}

void GSize::Set(int width, int height)
{
        SetWidth(width);
        SetHeight(height);
}

GSize &GSize::Scale(float xscale, float yscale)
{
        width *= xscale;
        height *= yscale;
        
        return *this;
}