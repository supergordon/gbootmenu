#ifndef _GGUI_
#define _GGUI_

#include "GSDK.h"

class GGui {
private:
public:
        GGui();
        ~GGui();
        
        bool InArea(int x, int y, int w, int h);
        bool IsNormalTap(int timeout = 1000);
        bool InAreaTap(int x, int y, int w, int h);
        bool InAreaHolding(int x, int y, int w, int h);
        
        bool Button(int x, int y, int min_w, int min_h, const char *title);
        
        void NotificationBar();
        void NavigationBar();
        
        void Render();
};

extern GGui *g_pGui;

#endif