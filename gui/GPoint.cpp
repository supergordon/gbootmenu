#include "GPoint.h"

GPoint::GPoint()
{
        x = -1;
        y = -1;
}

GPoint::GPoint(int x, int y)
{
        this->x = x;
        this->y = y;
}

void GPoint::operator =(const GPoint &point)
{
        x = point.x;
        y = point.y;
}

bool GPoint::operator ==(const GPoint &point)
{
        return x == point.x && y == point.y;
}

bool GPoint::operator !=(const GPoint &point)
{
        return x != point.x || y != point.y;
}

GPoint GPoint::operator +(const GPoint &point)
{
        x += point.x;
        y += point.y;
        
        return *this;
}
GPoint GPoint::operator -(const GPoint &point)
{
        x -= point.x;
        y -= point.y;
        
        return *this;
}

GPoint &GPoint::operator +=(const GPoint &point)
{
        x += point.x;
        y += point.y;
        
        return *this;
}
GPoint &GPoint::operator -=(const GPoint &point)
{
        x -= point.x;
        y -= point.y;
        
        return *this;
}

GPoint GPoint::operator +(const GSize &size)
{
        x += size.width;
        y += size.height;
        
        return *this;
}
GPoint GPoint::operator -(const GSize &size)
{
        x -= size.width;
        y -= size.height;
        
        return *this;
}

GPoint &GPoint::operator +=(const GSize &size)
{
        x += size.width;
        y += size.height;
        
        return *this;
}
GPoint &GPoint::operator -=(const GSize &size)
{
        x -= size.width;
        y -= size.height;
        
        return *this;
}