#ifndef __GPOINT__
#define __GPOINT__

#include "GSize.h"

class GPoint {
private:
       
public:
        GPoint();
        GPoint(int x, int y);
        
        void operator =(const GPoint &point);
        bool operator ==(const GPoint &point);
        bool operator !=(const GPoint &point);
        
        GPoint operator +(const GPoint &point);
        GPoint operator -(const GPoint &point);
        
        GPoint &operator +=(const GPoint &point);
        GPoint &operator -=(const GPoint &point);
        
        GPoint operator +(const GSize &size);
        GPoint operator -(const GSize &size);
        
        GPoint &operator +=(const GSize &size);
        GPoint &operator -=(const GSize &size);
        
        int x;
        int y;
};

const GPoint G_DefaultPosition = GPoint(-1, -1);

#endif
