#ifndef __GCONTROL__
#define __GCONTROL__

#include "GSDK.h"

class GControl {
private:
public:
        GControl();

        int x;
        int y;
        int w;
        int h;
        
        GColor background_color;
};

#endif
