#include "GSDK.h"

GGui *g_pGui = new GGui();


GGui::GGui()
{
        
}

GGui::~GGui()
{
        
}

bool GGui::InArea(int x, int y, int w, int h)
{
        touch_point *p = g_pDevice->IsEmulator() ? g_pSingleTouch->st
        : g_pMultiTouch->real_mt;
        
        if (p->x >= x && p->x < (x+w) &&
            p->y >= y && p->y < (y+h)) {
                return true;
        }
        
        return false;
}

bool GGui::IsNormalTap(int timeout)
{
        return g_pDevice->IsEmulator() ? g_pSingleTouch->IsNormalTap(timeout)
        : g_pMultiTouch->IsNormalTap(timeout);
}

bool GGui::InAreaTap(int x, int y, int w, int h)
{
        if (InArea(x, y, w, h)) {
                if (IsNormalTap())
                        return true;
        }
        return false;
}

bool GGui::InAreaHolding(int x, int y, int w, int h)
{
        if (InArea(x, y, w, h)) {
                bool ret = g_pDevice->IsEmulator() ? g_pSingleTouch->IsHolding()
                : g_pMultiTouch->IsHolding();
                
                if (ret)
                        return true;
        }
        return false;
}

bool GGui::Button(int x, int y, int min_w, int min_h, const char *title)
{
        int w = min_w ? min_w : (g_pGraphics->GetTextSize(title) + 50);
        int h = min_h ? min_h : 100;
        
        /*g_pDraw->FilledQuad(x, y, w, h, black);
         g_pDraw->Quad(x, y, w, h, 1, green);
         
         g_pDraw->Text(x+w/2, y+h/2+g_pDraw->GetFontHeight()/2,
         white, ALIGN_CENTER, title);*/
        
        g_pDraw->FilledQuad(x, y, w, h, GColor(200, 200, 200));
        
        g_pDraw->FilledQuad(x, y, w, 2, GColor(220, 220, 220)); //oben
        g_pDraw->FilledQuad(x, y+h, w+2, 2, GColor(170, 170, 170)); //unten
        
        g_pDraw->FilledQuad(x, y+2, 2, h-2, GColor(220, 220, 220)); //links
        g_pDraw->FilledQuad(x+w, y, 2, h, GColor(170, 170, 170)); //rechts
        
        if(InAreaHolding(x, y, w, h)) {
                g_pDraw->FilledQuad(x-8, y-8, w+16, h+16, GColor(74, 188, 230, 45));
                g_pDraw->FilledQuad(x, y, w, h, GColor(0, 120, 210, 75));
        }
        
        g_pDraw->Text(x+w/2, y+h/2+g_pDraw->GetFontHeight()/2,
                      color_black, ALIGN_CENTER, title);
        
        if (InAreaTap(x, y, w, h))
                return true;
        
        return false;
}

//notification bar height: 50px
//                 color: 0 0 0
void GGui::NotificationBar()
{
        g_pDraw->FilledQuad(0, 0, g_pGraphics->GetWidth(), 50, color_black);
        
        time_t time_cur = time(NULL);
        tm *tm_cur = localtime(&time_cur);
        tm_cur->tm_hour += 1;
        
        char buf_time[32];
        strftime(buf_time, 32, "%a.%H:%M", tm_cur);
        
        GColor textColor(40, 122, 169);
        
        g_pDraw->Text(g_pGraphics->GetWidth()-12, 25+g_pDraw->GetFontHeight()/2,
                      textColor, ALIGN_RIGHT, "%d%%  %s",
                      g_pDevice->GetBatteryCapacity(),
                      buf_time);
}

void GGui::NavigationBar()
{
        GColor navbarColor(34, 34, 34);
        GColor textColor(234, 234, 234);
        
        g_pDraw->FilledQuad(0, 50, g_pGraphics->GetWidth(), 96, navbarColor);
        
        static int time = GTimer::GetTickCount();
        
        g_pDraw->Text(30, 99+g_pDraw->GetFontHeight()/2, textColor,
                      ALIGN_LEFT, "GBootMenu v1.5.0 %i", GTimer::GetTickCount()-time);
}

class GQuad {
public:
        GQuad(int x, int y, int w = 0, int h = 0) {
                this->x = x;
                this->y = y;
                this->w = w;
                this->h = h;
        }
        void Render() { }
        int x, y, w, h;
private:
        
};

class GText : public GQuad {
public:
        GText(std::string text, int x, int y, const GColor &colors) : GQuad(x, y) {
                this->text = text;
                this->colors = colors;
        }
        void Render() {
                g_pDraw->Text(x, y, colors,
                              ALIGN_LEFT, "%s", text.c_str());
        }
        std::string text;
        GColor colors;
private:
};

class GPanel {
public:
        GPanel(int x, int y, int w, int h, GColor col) {
                this->pt.x = x;
                this->pt.y = y;
                this->sz.width = w;
                this->sz.height = h;
        }

        void Render() {
                g_pDraw->FilledQuad(pt.x, pt.y, sz.width, sz.height, colorBackground);
        }
        
private:
        GPoint pt;
        GSize sz;
        GColor colorBackground;
};

class GPage {
public:
        GPage(std::string title, int id) {
                this->title = title;
                this->id = id;
        }
        void Render() {
                GColor color_background(240, 240, 240);
                
                /* Background */
                g_pDraw->FilledQuad(0, 50, g_pGraphics->GetWidth(),
                                    g_pGraphics->GetHeight()-50,
                                    color_background);
                
                GColor navbarColor(34, 34, 34);
                GColor textColor(234, 234, 234);
                
                g_pDraw->FilledQuad(0, 50, g_pGraphics->GetWidth(), 96, navbarColor);

                g_pDraw->Text(30, 99+g_pDraw->GetFontHeight()/2, textColor,
                              ALIGN_LEFT, "%s", title.c_str());
                
                //panel rendern
        }
        void SetPanel(GPanel *panel);
private:
        std::string title;
        int id;
};

void GGui::Render()
{
        /*GPanel *panel = new GPanel(0, 0, g_pGraphics->GetWidth(), 50, color_green);
        panel->Render();
    
        GPage *page = new GPage("Page 1", 1);
        page->Render();
        delete page;*/
        
        GColor color_background(240, 240, 240);
        
        /* Background */
        g_pDraw->FilledQuad(0, 0, g_pGraphics->GetWidth(),
                           g_pGraphics->GetHeight(),
                           color_background);
        
        NotificationBar();
        
        int y = 220;

        if(Button(50, y, 0, 0, "Reboot to Android")) {
                g_pBootloader->Reboot(GBootloader::RESTART);
        } y += 120;
        
        if(Button(50, y, 0, 0, "Reboot to Recovery")) {
                g_pBootloader->Reboot(GBootloader::RESTART2, "recovery");
        } y += 120;
        
        if(Button(50, y, 0, 0, "Shutdown device")) {
                g_pBootloader->Reboot(GBootloader::POWEROFF);
        } y += 120;
}

// GColor color_background(240, 240, 240);

/* Background */
//g_pDraw->FilledQuad(0, 0, g_pGraphics->GetWidth(),
//                   g_pGraphics->GetHeight(),
//                   color_background);

//NotificationBar();

//int y = 220;

//*g_pCWM->value = Button(50, y, 0, 0, "Boot Clockworkmod Touch"); y += 100;
//*g_pTWRP->value = Button(50, y, 0, 0, "Boot Team Win Recovery Project"); y += 100;
//*g_pStockRecovery->value = Button(50, y, 0, 0, "Boot Stock Recovery"); y += 100;
//*g_pAromafm->value = Button(50, y, 0, 0, "Boot Aroma File Manager"); y += 100;
