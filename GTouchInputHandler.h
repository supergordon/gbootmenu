#ifndef __GTOUCHINPUTHANDLER__
#define __GTOUCHINPUTHANDLER__

#include "GSDK.h"

/*
 For devices that do not support the multi touch event protocol
 (only one finger touch is supported)
 */

struct touch_point {
        int x;
        int y;
        
        bool ispressing;
        bool isdragging;
        int velocity;
        int size;
};

class GSingleTouch {
private:
public:
        GSingleTouch();
        ~GSingleTouch();
        bool EventHandlePointer(EventHandleInput *ev);
        bool EventHandleTap(EventHandleInput *ev);
        
        void OnEventTapDown();
        void OnEventTapUp();
        void OnEventPointerMove(int code, int value);
        
        bool IsNormalTap(int timeout = 150);
        bool IsDoubleTap();
        bool IsHolding();
        
        touch_point *st;
};

/*
 For devices with multi touch event protocol support
 (up to 10 finger touch supported)
 */
class GMultiTouch {
private:
public:
        GMultiTouch();
        ~GMultiTouch();
        
        bool EventHandlePointer(EventHandleInput *ev);
        bool EventHandleTap(EventHandleInput *ev);
        
        void OnEventTapDown();
        void OnEventTapUp();
        void OnEventPointerMove(int code, int value);
        
        void VirtualToScreen(touch_point *virt, touch_point *real);
        
        bool IsNormalTap(int timeout = 1000);
        bool IsDoubleTap();
        bool IsHolding();
        
        touch_point *virtual_mt;
        touch_point *real_mt;
        
        GCondition condRelease;
};

extern GSingleTouch *g_pSingleTouch;
extern GMultiTouch *g_pMultiTouch;

#endif