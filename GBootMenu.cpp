#include "GSDK.h"

GBootMenu *g_pBootMenu = new GBootMenu;

GBootMenu::GBootMenu()
{
        szTitle = const_cast<char*>(GVER_STR);
}

GBootMenu::~GBootMenu()
{
        
}

char *GBootMenu::GetMenuTitle()
{
        return szTitle;
}

void GBootMenu::Init()
{
	g_pLog->Console("\n\nStarting GBootMenu...\n");
        g_pLog->Screen("GBootMenu initialized...");
        
        g_pFileSystemTable->ReadFromFile("/etc/recovery.fstab");
        g_pProcMounts->ReadFromFile("/proc/mounts");
        
       // g_pVolume->Mount("/data");
       // g_pVolume->Mount("/cache");
       // g_pVolume->Mount("/system");
        
        //g_pLog->Screen("Block specials read and mounted...");
        
        g_pProcMounts->Refresh();
	
        for (int i = 0; i < g_pProcMounts->GetCount(); i++) {
                GProcMounts::GProcMountsTable *m = g_pProcMounts->Get(i);
                
                if (!m) continue;
                
                g_pLog->Screen("Mounted: %s", m->mountpoint);
        }
        
        //g_pBootloader->Clear();
        //g_pLog->Screen("Bootloader message cleared...");
        
	g_pLog->Console("Func --> GBootMenu::Main");
	g_pLog->Console("Call --> GGraphics::Init");
	g_pGraphics->Init();
        g_pInput->Init();
        
        if (!g_pDevice->IsEmulator()) {
                g_pDevice->Init();
                g_pDevice->SetLcdBrightness(20);
                g_pDevice->UpdateStats();
        }
        else {
                g_pLog->Screen("Emulator detected...");
        }

        g_pGraphicsUpdater->Request();
        
	for (;;)
                usleep(10000);
}

void GBootMenu::Render()
{
	static bool bInit = false;
	if (!bInit) {
                g_pLog->Console("Call --> GDraw::SetContext");
		g_pDraw->SetContext(g_pGraphics->GetContext());
		
		g_pLog->Console("Call --> GMenu::Init");
		g_pMenu->Init();
                
                g_pLog->Console("Call --> GDevice::Init");
                
		bInit = true;
        }
        
        g_pDraw->FilledQuad(0, 0, g_pGraphics->GetWidth(),
                            g_pGraphics->GetHeight(),
                            color_black);
        
       // g_pMenu->Draw();
        g_pGui->Render();

        g_pDraw->FilledQuad(g_pDevice->IsEmulator() ? g_pSingleTouch->st->x
                            : g_pMultiTouch->real_mt->x,
                            g_pDevice->IsEmulator() ? g_pSingleTouch->st->y
                            : g_pMultiTouch->real_mt->y,
                            15, 15,
                            (g_pDevice->IsEmulator() ? g_pSingleTouch->st->ispressing
                             : g_pMultiTouch->real_mt->ispressing)
                            ?color_red:color_blue);
        
        g_pDevice->UpdateStats();
        g_pDraw->Text(12, 25+g_pDraw->GetFontHeight()/2,
                      color_white, ALIGN_LEFT, "BV[%.2fV] BT[%.1fC] BC[%d%%] CT[%.1fC] LCD[%d%%]",
                      g_pDevice->GetBatteryVoltage(),
                      g_pDevice->GetBatteryTemp(),
                      g_pDevice->GetBatteryCapacity(),
                      g_pDevice->GetCpuTemp(),
                      g_pDevice->GetLcdBrightness());
      
        g_pLog->ScreenRender();
}
