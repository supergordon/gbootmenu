#include "GSDK.h"

GMenu *g_pMenu = new GMenu();

GMenuElement *g_pCWM = new GMenuElement("Boot Clockworkmod Touch", "Select to boot Clockworkmod Touch");
GMenuElement *g_pTWRP = new GMenuElement("Boot Team Win Recovery Project", "Select to boot TWRP");
GMenuElement *g_pStockRecovery = new GMenuElement("Boot Stock Recovery", "Select to boot Stock Recovery");
GMenuElement *g_pAromafm = new GMenuElement("Start Aroma Filemanager", "Select to start Aroma Filemanager");
GMenuElement *g_pFastboot = new GMenuElement("Boot Fastboot", "Select to boot Fastboot");
GMenuElement *g_pReboot = new GMenuElement("Reboot System", "Select to reboot system");

void GMenu::Init()
{
	s = 0;
	
	this->title = g_pBootMenu->GetMenuTitle();
	
	this->AddEntry(g_pCWM);
	this->AddEntry(g_pTWRP);
                this->AddEntry(g_pStockRecovery);
	this->AddEntry(g_pAromafm);
	this->AddEntry(g_pFastboot);
	this->AddEntry(g_pReboot);
}

GMenu::~GMenu()
{
        for (v_it = v.begin(); v_it != v.end(); v_it++)
                delete [] *v_it;
                
        v.clear();
}

void GMenu::AddEntry(GMenuElement *item)
{
	v.push_back(item);
}

bool GMenu::KeyHandling(int key)
{
        int c = v.size();
        
	if (key == GVK_VOLUP) {
		if(s <= 0) s = c-1;
		else s--;
		return true;
	}
	if (key == GVK_VOLDOWN) {
		if(s >= (c-1)) s = 0;
		else s++;
		return true;
	}
	
	if (key == GVK_POWER) {
		*v.at(s)->value = !*v.at(s)->value;
		return true;
	}
	return false;
}

void GMenu::Draw()
{
	int c = v.size();
        
	g_pDraw->Text(g_pGraphics->GetWidth()/2, g_pDraw->GetFontHeight()*1.5,
                      color_green, ALIGN_CENTER, this->title);
        
	for (int i = 0; i < c; i++) {
		int abstand = g_pDraw->GetFontHeight()*0.7;
                
		if (i == s) {
			g_pDraw->Text(x+50, y+200+i * (g_pDraw->GetFontHeight() + abstand),
                                      color_green, ALIGN_LEFT, v.at(i)->entry);
			
			if (v.at(i)->desc) {
				g_pDraw->FilledQuad(0, g_pGraphics->GetHeight()-g_pDraw->GetFontHeight()*2, g_pGraphics->GetHeight(), g_pDraw->GetFontHeight()*2, GColor(0, 255, 0, 80));
				g_pDraw->Text(g_pGraphics->GetWidth()/2, g_pGraphics->GetHeight()-15, color_white, ALIGN_CENTER, v.at(i)->desc);
			}
                        
		}
		if (i != s) {
			if (*v.at(i)->value)
				g_pDraw->Text(x+50, y+200+i * (g_pDraw->GetFontHeight() + abstand), color_yellow, ALIGN_LEFT, v.at(i)->entry);
			else
				g_pDraw->Text(x+50, y+200+i * (g_pDraw->GetFontHeight() + abstand), color_white, ALIGN_LEFT, v.at(i)->entry);
		}
                
                g_pGraphicsUpdater->Request();
	}
}