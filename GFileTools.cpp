#include "GSDK.h"

GFileTools *g_pFileTools = new GFileTools;

bool GFileTools::isPath(const char *path)
{
        struct stat st;
        if (stat(path, &st) == 0)
                return true;
        
        return false;
}

bool GFileTools::isSymlink(const char *path)
{  
        struct stat st;
        if (lstat(path, &st) == 0)
                if (S_ISLNK(st.st_mode))
                        return true;
        
        return false;
}

bool GFileTools::isDirectory(const char *path)
{
        struct stat st;
        if (stat(path, &st) == 0)
                if (S_ISDIR(st.st_mode))
                        return true;
        
        return false;
}

bool GFileTools::isRegular(const char *path)
{
        struct stat st;
        if (stat(path, &st) == 0)
                if (S_ISREG(st.st_mode))
                        return true;
        
        return false;
}

bool GFileTools::isBlock(const char *path)
{
        struct stat st;
        if (stat(path, &st) == 0)
                if (S_ISBLK(st.st_mode))
                        return true;
        
        return false;
}

bool GFileTools::isCharacter(const char *path)
{
        struct stat st;
        if (stat(path, &st) == 0)
                if (S_ISCHR(st.st_mode))
                        return true;
        
        return false;
}

char *GFileTools::resolveSymlink(const char *path)
{
        if (!isSymlink(path)) 
                return 0;

        char *buf = new char[256];
        
        if (readlink(path, buf, 256) != -1) {
                if (buf[0] != '/') {
                        char *ret = strdup(buf);
                        memset(buf, 0, 256);
                        
                        sprintf(buf, "/%s", ret);
                        delete [] ret;
                        
                        return buf;
                }
                else {
                        return buf;
                }
        }
        else {
                delete [] buf;
                return 0;
        }
}

unsigned long long GFileTools::getFileSize(const char *path)
{ 
        struct stat st;
        if (stat(path, &st) == 0)
                return static_cast<unsigned long long>(st.st_size);
        
        return 0;
}

unsigned long long GFileTools::getFolderSize(const char *path)
{
        DIR *handle;
        struct dirent *dir;
        
        handle = opendir(path);
        if (!handle)
                return 0;
        
        unsigned long long size = 0;
        while ((dir = readdir(handle))) {
                if (strcmp(dir->d_name, ".") == 0 ||
                   strcmp(dir->d_name, "..") == 0)
                        continue;
                        
                char *buf = new char[1024];
                sprintf(buf, "%s/%s", path, dir->d_name);
                
                if (dir->d_type == DT_REG)
                        size += getFileSize(buf);
                else if (dir->d_type == DT_DIR)
                        size += getFolderSize(buf);
                
                delete [] buf;
        }
        
        closedir(handle);
        return size;
}