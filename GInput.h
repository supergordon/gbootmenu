#ifndef __GINPUT__
#define __GINPUT__

#include "GSDK.h"

class EventInput;
class EventHandleInput;

class GInput {
public:
        GInput();
        ~GInput();
        
        void Init();

        struct touch_abs {
                signed int value;
                signed int minimum;
                signed int maximum;
                signed int fuzz;
                signed int flat;
                signed int resolution;
        };
        
        static const int MAX_EVENT_INPUT_COUNT = 32;
        
        int EventInit();
        int EventWait(int timeout);
        
        int GetVirtualWidth();
        int GetVirtualHeight();
        
        EventInput **event_input;
        pollfd event_input_poll[MAX_EVENT_INPUT_COUNT];
        int event_input_count;
private:
	static void *EventInputThread(void *this_ptr);
        void EventProcessInput(EventHandleInput *ev);
        
        GTimer::Measure m;
        touch_abs **ta;
};

class EventInput {
public:
        EventInput()
        {
                memset(&time, 0, sizeof(timeval));
                type = 0;
                code = 0;
                value = 0;
        }
        EventInput(timeval *ti,
                   unsigned short ty, unsigned short co, unsigned short va)
        {
                SetTime(ti);
                SetType(ty);
                SetCode(co);
                SetValue(va);
        }
        timeval *GetTime() { return &time; }
        unsigned short GetType() const { return type; }
        unsigned short GetCode() const { return code; }
        signed int GetValue() const { return value; }
        
        void SetTime(timeval *time)
        {
                if (time) memcpy(&this->time, time, sizeof(timeval));
        }
        void SetType(unsigned short type) { this->type = type; }
        void SetCode(unsigned short code) { this->code = code; }
        void SetValue(signed int value) { this->value = value; }
private:
        timeval time;
	unsigned short type;
	unsigned short code;
	signed int value;
};

class EventHandleInput : public EventInput {
public:
        EventHandleInput()
        : EventInput()
        {
                
        }
        EventHandleInput(timeval *ti,
                         unsigned short ty, unsigned short co, unsigned short va)
        : EventInput(ti, ty, co, va)
        {
                
        }
        
        bool IsSyn() { return GetType() == EV_SYN; }
        bool IsKey() { return GetType() == EV_KEY; }
        bool IsRel() { return GetType() == EV_REL; }
        bool IsAbs() { return GetType() == EV_ABS; }
        bool IsType(unsigned short type) { return GetType() == type; }
        
        /*
         When a key is depressed, an event with
         the key's code is emitted with value 1. When the key is released,
         an event is emitted with value 0.
         
         https://www.kernel.org/doc/Documentation/input/event-codes.txt
         */
        bool IsKeyCode(unsigned short code)
        {
                return GetCode() == code && GetValue() == 0;
        }
        
      private:
};

extern GInput *g_pInput;

#endif
