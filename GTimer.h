#ifndef __GTIMER__
#define __GTIMER__

class GTimer {
public:
        GTimer(int value);
        void Start();
        void Stop();
        bool Finished();
        bool Running();
        void Restart();
        
        bool Started();
        bool started;
        bool stopped;
        
        static int GetTickCount();
        
        class Measure {
        private:
                unsigned int start;
                unsigned int stopped_at;
        public:
                Measure();
                void Start();
                void Stop();
                void Reset();
                unsigned int ElapsedTime();
                unsigned int ElapsedStopTime();
                unsigned int InitialTime();
        };
                Measure m;
private:
        unsigned int start;
        unsigned int msec;

};

#endif
