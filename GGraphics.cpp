#include "GSDK.h"
#include "FontRes.h"

GGraphics *g_pGraphics = new GGraphics;

//P880
//int PIXEL_FORMAT = GGL_PIXEL_FORMAT_RGBX_8888;
//int PIXEL_SIZE = 4;

//X5001
int PIXEL_FORMAT = GGL_PIXEL_FORMAT_RGB_565;
int PIXEL_SIZE = 2;

#define NUM_BUFFERS 2

pthread_mutex_t g_RenderMutex;

font_t font = {
	1440, 24,
	15, 24,
        NULL
};

int GGraphics::GetFramebuffer(GGLSurface *fb)
{
        int fd = open("/dev/graphics/fb0", O_RDWR);
        if (fd < 0) {
                perror("cannot open fb0");
                return -1;
        }
        
        if (ioctl(fd, FBIOGET_VSCREENINFO, &vi) < 0) {
                perror("failed to get fb0 info");
                close(fd);
                return -1;
        }
        
        if(g_pDevice->IsEmulator()) {
                PIXEL_FORMAT = GGL_PIXEL_FORMAT_RGB_565;
                PIXEL_SIZE = 2;
        }
        
        vi.bits_per_pixel = PIXEL_SIZE * 8;
        if (PIXEL_FORMAT == GGL_PIXEL_FORMAT_BGRA_8888) {
                vi.red.offset     = 8;
                vi.red.length     = 8;
                vi.green.offset   = 16;
                vi.green.length   = 8;
                vi.blue.offset    = 24;
                vi.blue.length    = 8;
                vi.transp.offset  = 0;
                vi.transp.length  = 8;
        } else if (PIXEL_FORMAT == GGL_PIXEL_FORMAT_RGBX_8888) {
                vi.red.offset     = 24;
                vi.red.length     = 8;
                vi.green.offset   = 16;
                vi.green.length   = 8;
                vi.blue.offset    = 8;
                vi.blue.length    = 8;
                vi.transp.offset  = 0;
                vi.transp.length  = 8;
        } else { /* RGB565*/
                vi.red.offset     = 11;
                vi.red.length     = 5;
                vi.green.offset   = 5;
                vi.green.length   = 6;
                vi.blue.offset    = 0;
                vi.blue.length    = 5;
                vi.transp.offset  = 0;
                vi.transp.length  = 0;
        }
        if (ioctl(fd, FBIOPUT_VSCREENINFO, &vi) < 0) {
                perror("failed to put fb0 info");
                close(fd);
                return -1;
        }
        
        if (ioctl(fd, FBIOGET_FSCREENINFO, &fi) < 0) {
                perror("failed to get fb0 info");
                close(fd);
                return -1;
        }
        
        GGLubyte *bits = static_cast<GGLubyte*>(mmap(0, fi.smem_len,
                                                     PROT_READ | PROT_WRITE,
                                                     MAP_SHARED, fd, 0));
        if (bits == MAP_FAILED) {
                perror("failed to mmap framebuffer");
                close(fd);
                return -1;
        }
        
        fb->version = sizeof(*fb);
        fb->width = vi.xres;
        fb->height = vi.yres;
        fb->stride = fi.line_length/PIXEL_SIZE;
        fb->data = bits;
        fb->format = PIXEL_FORMAT;
        memset(fb->data, 0, vi.yres * fi.line_length);
        
        fb++;
        
        if (vi.yres * fi.line_length * 2 > fi.smem_len)
                return fd;
        
        double_buffering = 1;
        
        fb->version = sizeof(*fb);
        fb->width = vi.xres;
        fb->height = vi.yres;
        fb->stride = fi.line_length/PIXEL_SIZE;
        fb->data = static_cast<GGLubyte*>(bits + vi.yres * fi.line_length);
        fb->format = PIXEL_FORMAT;
        memset(fb->data, 0, vi.yres * fi.line_length);
        
        return fd;
}

void GGraphics::GetMemorySurface(GGLSurface* ms)
{
        ms->version = sizeof(*ms);
        ms->width = vi.xres;
        ms->height = vi.yres;
        ms->stride = fi.line_length/PIXEL_SIZE;
        ms->data = static_cast<GGLubyte*>(malloc(fi.line_length * vi.yres));
        ms->format = PIXEL_FORMAT;
}

void GGraphics::SetActiveFramebuffer(unsigned int n)
{
        if (n > 1 || !double_buffering) return;
        vi.yres_virtual = vi.yres * 2;
        vi.yoffset = n * vi.yres;
        vi.bits_per_pixel = PIXEL_SIZE * 8;
        if (ioctl(fb_fd, FBIOPUT_VSCREENINFO, &vi) < 0) {
                perror("active fb swap failed");
        }
}

void GGraphics::Flip()
{
        if (double_buffering)
                active_fb = (active_fb + 1) & 1;
        
        memcpy(framebuffer[active_fb].data, mem_surface.data,
               fi.line_length * vi.yres);
        
        /* inform the display driver */
        SetActiveFramebuffer(active_fb);
}

int GGraphics::GetTextSize(const char *s)
{
        return this->font->cwidth * strlen(s);
}

void GGraphics::GetFontSize(int *x, int *y)
{
        *x = this->font->cwidth;
        *y = this->font->cheight;
}

int GGraphics::SetText(int x, int y, const char *s)
{
        unsigned int off;
        y -= this->font->ascent;
        
        gl->bindTexture(gl, &this->font->texture);
        gl->texEnvi(gl, GGL_TEXTURE_ENV, GGL_TEXTURE_ENV_MODE, GGL_REPLACE);
        gl->texGeni(gl, GGL_S, GGL_TEXTURE_GEN_MODE, GGL_ONE_TO_ONE);
        gl->texGeni(gl, GGL_T, GGL_TEXTURE_GEN_MODE, GGL_ONE_TO_ONE);
        gl->enable(gl, GGL_TEXTURE_2D);
        
        while ((off = *s++)) {
                off -= 32;
                if (off < 96) {
                        gl->texCoord2i(gl, (off * this->font->cwidth) - x, 0 - y);
                        gl->recti(gl, x, y, x + this->font->cwidth, y + this->font->cheight);
                }
                x += this->font->cwidth;
        }
        
        return x;
}

void GGraphics::InitFont()
{
        unsigned char *in, data;
        
        this->font = new GRFont;
        GGLSurface *ftex = &this->font->texture;
        
        GGLubyte *bits = new GGLubyte[::font.width * ::font.height];
        
        ftex->version = sizeof(*ftex);
        ftex->width = ::font.width;
        ftex->height = ::font.height;
        ftex->stride = ::font.width;
        ftex->data = bits;
        ftex->format = GGL_PIXEL_FORMAT_A_8;
        
        in = static_cast<unsigned char *>(fontres);
        while ((data = *in++)) {
                memset(bits, (data & 0x80) ? 255 : 0, data & 0x7f);
                bits += (data & 0x7f);
        }
        
        this->font->cwidth = ::font.cwidth;
        this->font->cheight = ::font.cheight;
        this->font->ascent = ::font.cheight - 2;
}

int GGraphics::InitGraphics()
{
        gglInit(&gl);
        
        InitFont();
        vt_fd = open("/dev/tty0", O_RDWR | O_SYNC);
        if (vt_fd < 0) {
                
        } else if (ioctl(vt_fd, KDSETMODE, (void*) KD_GRAPHICS)) {
                Exit();
                return -1;
        }
        
        fb_fd = GetFramebuffer(framebuffer);
        if (fb_fd < 0) {
                Exit();
                return -1;
        }
        
        GetMemorySurface(&mem_surface);
        
        /* start with 0 as front (displayed) and 1 as back (drawing) */
        active_fb = 0;
        SetActiveFramebuffer(0);
        gl->colorBuffer(gl, &mem_surface);
        
        gl->activeTexture(gl, 0);
        gl->enable(gl, GGL_BLEND);
        gl->blendFunc(gl, GGL_SRC_ALPHA, GGL_ONE_MINUS_SRC_ALPHA);
        
        //Blank(true);
       // Blank(false);
        
        return 0;
}

void GGraphics::Exit()
{
        close(fb_fd);
        fb_fd = -1;
        
        free(mem_surface.data);
        
        ioctl(vt_fd, KDSETMODE, (void*) KD_TEXT);
        close(vt_fd);
        vt_fd = -1;
}

void GGraphics::Blank(bool blank)
{
        ioctl(fb_fd, FBIOBLANK, blank ? FB_BLANK_POWERDOWN : FB_BLANK_UNBLANK);
}

GGraphicsUpdater *g_pGraphicsUpdater = new GGraphicsUpdater();

void *GGraphics::RenderThread(void *this_ptr)
{
    GGraphics *g = static_cast<GGraphics*>(this_ptr);
    g_pGraphicsUpdater->Request();
        
    for(;;) {
            
            if(0) {
                    g_pBootMenu->Render();
                    g->Flip();
            }
            else {
                    static GTimer t(2000);
                    
                    if(g_pGraphicsUpdater->NeedsRender() || t.Running()) {
                            
                            if(!t.Started())
                                    t.Restart();

                            g_pBootMenu->Render();
                            g->Flip();
                            g_pGraphicsUpdater->Clear();
                    }
                    else {
                            usleep(5000);
                    }
            }
	}
        
	return NULL;
}

void GGraphics::Init()
{
	g_pLog->Console("Func --> GGraphics::Init");
        
	g_pLog->Console("Call --> gglInit");
	InitGraphics();
        
        pthread_t t;
        pthread_create(&t, NULL, RenderThread, this);
}

void GGraphics::Destroy()
{
        gglUninit(gl);
}

GGLContext* GGraphics::GetContext()
{
	return gl;
}

int GGraphics::GetWidth() const
{
	return framebuffer[0].width;
}

int GGraphics::GetHeight() const
{
	return framebuffer[0].height;
}

int GGraphics::GetVirtualWidth() const
{
	return framebuffer[0].width;
        //return framebuffer[0].width*2
}

int GGraphics::GetVirtualHeight() const
{
	return framebuffer[0].height;
        //return framebuffer[0].height*2
}
