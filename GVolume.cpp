#include "GSDK.h"

GVolume *g_pVolume = new GVolume();

GVolume::GVolume()
{
        
}

GVolume::~GVolume()
{
        
}

bool GVolume::IsMounted(const char *path)
{
        g_pProcMounts->Refresh();
        
        for (register int i = 0; i < g_pProcMounts->GetCount(); i++) {
                GProcMounts::GProcMountsTable *m = g_pProcMounts->Get(i);
                
                if (!m)
                        continue;
                
                if (strcmp(path, m->mountpoint) == 0)
                        return true;
        }
        return false;
}

GVolume::GVolumes *GVolume::Get(const char *path)
{
        for (register int i = 0; i < g_pFileSystemTable->GetCount(); i++) {
                GVolume::GVolumes
                *m = static_cast<GVolume::GVolumes*>(g_pFileSystemTable->Get(i));
                
                if (!m)
                        continue;
                
                if (strcmp(path, m->mountpoint) == 0)
                        return m;
        }
        return 0;
}

bool GVolume::Mount(const char *path)
{
        if (IsMounted(path))
                return true;
        
        GVolume::GVolumes *m = Get(path);
        if (!m)
                return false;
        
        mount(m->device, m->mountpoint,
              g_pFileSystemTable->ConvertType(m->fstype),
              MS_NOATIME | MS_NODEV | MS_NODIRATIME, "");
        
        return true;
}

bool GVolume::Unmount(const char *path, bool force)
{
        if (!IsMounted(path))
                return true;
        
        GVolume::GVolumes *m = Get(path);
        if (!m)
                return false;
        
        umount2(m->mountpoint, force?MNT_FORCE:0);
        
        return true;
}

bool GVolume::UnmountAll()
{
        g_pProcMounts->Refresh();
        
        for (register int i = 0; i < g_pProcMounts->GetCount(); i++) {
                GProcMounts::GProcMountsTable *m = g_pProcMounts->Get(i);
                
                if (!m) continue;
                
                Unmount(m->mountpoint, true);
        }
        return true;
}