#ifndef __GFILESYSTEMTABLE__
#define __GFILESYSTEMTABLE__

#include "GSDK.h"

enum GFileSystemType {
        NONE,
        EXT2,
        EXT3,
        EXT4,
        EMMC,
        MTD,
        VFAT,
        EXFAT,
        DATAMEDIA,
        FUSE,
        ROOTFS,
        TMPFS,
        DEVPTS,
        PROC,
        SYSFS,
        DEBUGFS
};

class GFileSystemTable
{
public:
        class GFileSystemTableContent {
        private:
        public:
                GFileSystemTableContent()
                {
                        mountpoint = new char[256];
                        device = new char[256];
                        device2 = new char[256];
                        options = new char[256];
                        
                        memset(mountpoint, 0, 256);
                        memset(device, 0, 256);
                        memset(device2, 0, 256);
                        memset(options, 0, 256);
                        
                        fstype = NONE;
                }
                
                ~GFileSystemTableContent()
                {
                        delete [] mountpoint;
                        delete [] device;
                        delete [] device2;
                        delete [] options;
                }
                
                char *mountpoint;
                char fstype;
                char *device;
                char *device2;
                char *options;
        };
private:
        std::vector<GFileSystemTableContent*> vfst;
        std::vector<GFileSystemTableContent*>::iterator vfst_it;
        
        GFileSystemTableContent *ConvertLine(const char* line);
public:
        GFileSystemTable();
        ~GFileSystemTable();
        
        int ReadFromFile(const char* filepath);
        int Add(GFileSystemTableContent* fstc);
        int GetCount();
        GFileSystemTableContent *Get(int id);
        
        bool ValidateLine(const char* line, bool mtab = false);
        char ConvertType(const char* fs);
        char *ConvertType(const char type);
};

extern GFileSystemTable *g_pFileSystemTable;

#endif