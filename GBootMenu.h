#ifndef __GBOOTMENU__
#define __GBOOTMENU__

#include "GSDK.h"


class GBootMenu {
private:
        char *szTitle;
public:
	GBootMenu();
        ~GBootMenu();
	
	void Init();
	void Render();
	
	char *GetMenuTitle();
};

extern GBootMenu *g_pBootMenu;

#endif
